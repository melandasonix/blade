@extends('adminlte.master')
@section('content1')
<div class="mt-3 ml-3">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">Fixed Header Table</h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
          {{-- <div class="card-tools"> --}}
            
          {{-- </div> --}}
          <!-- /.card-header -->
          <div class="card-body">
              @if(session('sukses'))
              <div class="alert alert-success">
                  {{session('sukses')}}
              </div>
              @endif
              <a class="btn btn-info mb-2" href="/pertanyaan/create">Create</a>
            <table class="table table-bordered">
              <thead>                  
                <tr>
                  <th style="width: 10px">No</th>
                  <th>Judul</th>
                  <th>Isi</th>
                  <th>Tanggal dibuat</th>
                  <th>Tanggal diperbaharui</th>
                  <th>Pertanyaan id</th>
                  <th>Action                  </th>

                </tr>
              </thead>
              <tbody>
                {{-- <tr>
                  <td>1.</td>
                  <td>Update software</td>
                  <td>Update software</td>
                  
                  <td>Update software</td>
                  <td>Update software</td>
                  <td>Update software</td>
                  <td><span class="badge bg-danger"></span></td>
                </tr> --}}
                {{-- @foreach ($pertanyaan as $key => $pertanyaan) --}}
                {{-- atau --}}
                @forelse ($pertanyaan as $key => $pertanyaan)

                <tr>
                    <td> {{$key + 1 }} </td>
                    <td> {{$pertanyaan->judul }} </td>
                    <td> {{$pertanyaan->isi }} </td>
                    <td> {{$pertanyaan->tanggal_dibuat }} </td>
                    <td> {{$pertanyaan->tanggal_diperbaharui }} </td>
                    <td> {{$pertanyaan->pertanyaan_id }} </td>
                    <td style="display:flex; "> 
                      <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info btn-sm"> show                      </a>
                      <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-warning btn-sm"> edit                     </a>
                      <form action="/pertanyaan/{{$pertanyaan->id}}" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">

                      </form>
                      {{-- <form action="{{ route('category.destroy', $value->id ) }}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="{{ route('category.edit', $value->id) }}" class="btn btn-primary btn-sm my-1">Edit</a>
                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                     </form> --}}
                    </td>
                    
                    
                </tr>
                @empty
                    <tr>
                        <td colspan="12" align="center"> Data kosong </td>
                    </tr>                        
                    {{-- @endempty --}}
                @endforelse
                {{-- atau --}}
                {{-- @endforeach --}}

              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
          <div class="card-footer clearfix">
            <ul class="pagination pagination-sm m-0 float-right">
              <li class="page-item"><a class="page-link" href="#">«</a></li>
              <li class="page-item"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item"><a class="page-link" href="#">»</a></li>
            </ul>
          </div>
        </div>
        <!-- /.card -->
        <!-- /.card -->
      </div>

</div>
    
@endsection