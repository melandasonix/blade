@extends('adminlte.master')
@section('content1')
{{-- form tabel pertanyaan --}}
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Create Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan" method="POST">
            @csrf
        <div class="card-body">
            <div class="form-group">
            <label for="judul">Judul</label>
            <input type="text" class="form-control" id="judul" name="judul" placeholder="Enter judul">
            {{-- pemberitahuan error --}}
            </div>
            <div class="form-group">
            <label for="isi">Isi</label>
            <input type="text" class="form-control" id="isi" name="isi" placeholder="Enter isi">
            
            </div>
            <div class="form-group">
                <label for="tanggal_dibuat">Tanggal dibuat</label>
                <input type="date" class="form-control" id="tanggal_dibuat" name="tanggal_dibuat" placeholder="Enter tanggal_dibuat">
            </div>        
            <div class="form-group">
                <label for="tanggal_diperbaharui">Tanggal diperbaharui</label>
                <input type="date" class="form-control" id="tanggal_diperbaharui" name="tanggal_diperbaharui" placeholder="Enter tanggal_diperbaharui">
            </div>
            <div class="form-group">
                <label for="pertanyaa_id">Pertanyaan id</label>
                <input type="text" class="form-control" id="pertanyaan_id" name="pertanyaan_id" placeholder="Enter pertanyaan_id">
            </div>
            
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
        </div>
        </form>
    </div>

{{-- /form tabel pertanyaan  --}}
    
@endsection