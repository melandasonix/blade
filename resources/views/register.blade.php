<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="img/clipboard.png" class="rel">
    <title>Form</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h4>Sign Up Form</h4>
    {{-- <form action="/kirim" method="POST"> --}}
    <form action="/welcome" method="POST">

        @csrf
            <legend>Nama</legend>
            <p for="">First name:</p>
            <input type="text" name="firstname">
            <p>Last name:</p>
            <input type="text" name="lastname">

        <fieldset>
            <legend>Pilih data</legend>
            <p>Gender:</p>
            <input type="radio" name="gender">Male <br>
            <input type="radio" name="gender">Female <br>
            <input type="radio" name="gender">Other
            <p>Nationality</p>
            <select name="" id="">
        <option value="">Indonesia</option>
        <option value="">Malaysia</option>
        </select>

            <p>Language Spoken</p>
            <input type="checkbox" name="Language"> Bahasa Indonesia <br>
            <input type="checkbox" name="Language"> English <br>
            <input type="checkbox"> Other
        </fieldset>
        <fieldset>
            <legend>Isi data</legend>
            <p>Bio: </p>
            <textarea name="" id="" cols="30" rows="10"></textarea><br>
        </fieldset>
        <input type="submit" onclick="home" value="kirim">

    </form>
    
    {{-- <form action="/kirim">
        <input type="submit" onclick="home" value="kirim">
    </form> --}}
</body>

</html>