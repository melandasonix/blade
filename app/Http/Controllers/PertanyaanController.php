<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// menambahkan untuk koneksi ke tabel database 
use DB;

class PertanyaanController extends Controller
{
    //
    public function create(){
        return view('pertanyaan.create');
    }
   
    public function store(Request $request){
        // dd($request->all());
        // return view('pertanyaan.create');

// menampilkan validasi permberitahuan jika calue kosong
        // $query->validate([
        //     'judul'=>'required|unique:pertanyaan',
        //     'isi' => 'required',
        //     'tanggal_dibuat' =>'required',
        //     'tanggal_diperbaharui' => 'required',
        //     'pertanyaan_id' => 'required'        ]);

        $query = DB::table('pertanyaan1')->insert([
            "judul" =>$request["judul"],
            "isi" => $request["isi"],
            "tanggal_dibuat" => $request["tanggal_dibuat"],
            "tanggal_diperbaharui" =>$request["tanggal_diperbaharui"],
            "pertanyaan_id" =>$request["pertanyaan_id"]

        ]);
        // memastikan tampil atau tidak
        return redirect('/pertanyaan')->with('sukses', 'data berhasil disimpan');
        // return redirect('/pertanyaan/create');


    }
    public function index(){
        $pertanyaan = DB:: table('pertanyaan1')->get();
        // dd($pertanyaan);
        return view('pertanyaan.index', compact('pertanyaan') );
    }
    public function show($id){
        $pertanyaan = DB:: table('pertanyaan1')->where('id', $id)->first();
        // dd($pertanyaan);
        return view('pertanyaan.show', compact('pertanyaan'));
    }
    public function edit($id){
        $pertanyaan = DB:: table('pertanyaan1')->where('id', $id)->first();
        // dd($pertanyaan);
        return view('pertanyaan.edit', compact('pertanyaan'));
    }
    public function update($id, Request $request){
        $query = DB::table('pertanyaan1')
                    ->where('id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi' => $request['isi'],
                        'tanggal_dibuat' =>$request['tanggal_dibuat'],
                        'tanggal_diperbaharui' =>$request['tanggal_diperbaharui'],
                        'pertanyaan_id' =>$request['pertanyaan_id']
                    ]);
        return redirect('/pertanyaan')->with('sukses', 'berhasil update');
    }
    public function destroy($id){
        $query = DB::table('pertanyaan1')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('sukses', 'berhasil dihapus');

    }
   
}

